package banco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import banco.Account;

public class Controller {
	Client onlyClient;
	ArrayList<Account> accounts;
	
	
	public Controller(){
		onlyClient = new Client("Jostin","Marin",1);
		accounts = new ArrayList<Account>() ;
	}
	
	public void menu() throws IOException, ParseException{
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		boolean salir = false;
		String opcion;
		String opcion2;
		Account account = null;
		while(salir==false){
			
			System.out.println("\n\n\nSistema Bancario\n-----------------");
			System.out.println("1.Crear Cuenta");
			System.out.println("2.Mostrar Cuentas");
			System.out.println("3.Retiro");
			System.out.println("4.Deposito");
			System.out.println("5.Compras Comercio");
			System.out.println("6.Generar Movimientos");
			System.out.println("7.Estado de cuenta");
			System.out.println("8.Salir");
			System.out.print("Elige una opcion:");
			opcion = buffer.readLine();
			
			switch(opcion){
				case "1":
					//create account
					System.out.println("1. Cuenta Ahorros");
					System.out.println("2. Cuenta Corriente\n");
					System.out.print("Seleccion el tipo de cuenta: ");
					opcion2 = buffer.readLine();
					switch(opcion2){
						case "1":
							createAccount(1);
							break;
						case "2":
							createAccount(2);
							break;
						default:
							System.out.println("Opcion invalida\n");
							break;
					}
					break;
				case "2":
					//mostrarCuentas
					viewAccounts();
					break;
				case "3":
					//retiro
					viewAccounts();
					makeMovement(1);
					break;
				case "4":
					//Deposito
					viewAccounts();
					makeMovement(2);
					break;
				case "5":
					//compraComercio
					viewAccounts();
					makeMovement(3);
					break;
				case "6":
					//movimientos
					viewAccounts();
					System.out.print("Digite el numero de cuenta: ");
					opcion = buffer.readLine();
					account = selectAccount(Integer.parseInt(opcion));
					generateTransactions(account);
					break;
				case "7":
					//estados
					viewAccounts();
					System.out.print("Digite el numero de cuenta: ");
					opcion = buffer.readLine();
					account = selectAccount(Integer.parseInt(opcion));
					account.printTransactions();
					break;
				case "8":
					salir = true;
					System.out.println("Proceso Terminado...");
					break;
				default:
					System.out.print("Opcion incorrecta\n");
					break;

			}
		}
	}
	
	public void createAccount(int type){
		Account account;
		//ahorros
		if(type==1){
			System.out.println("Ahorro");
			account = new Savings(onlyClient, 0.10, 0.5) ;
		}
		//corriente
		else{
			System.out.println("Corriente");
			account = new Regular(onlyClient, 0.15, 0.10, 50) ;
		}
		//add account
		accounts.add(account);
	};
	
	public void viewAccounts(){
		System.out.println("----------");
		System.out.println("Cuentas");
		System.out.println("----------\n");
		for(Account account : accounts){
			System.out.println(account.toString());
			System.out.println();
		}
	}
	

	public void generateTransactions(Account pAccount) throws ParseException{
		System.out.println("Generating random transactions");
		System.out.println("----------\n");
		
		Random r = new Random();
		int operation;
		for (int i = 0;i < r.nextInt(15-1)-1;i++){
			operation = r.nextInt(1-0)-0;
			if (operation == 0){
				pAccount.store(0 + (200000 - 0) * r.nextDouble());
			}else if (operation == 1){
				pAccount.withdraw(0 + (pAccount.getBalance() - 0) * r.nextDouble());
			}else if (operation == 2){
				pAccount.purchase(0 + (pAccount.getBalance() - 0) * r.nextDouble());
			}
		}
		System.out.println("Completed");
		System.out.println("----------\n");
	}
		
		/*
		int day;
			int month;
			String date;
			for (int i = 0;i < r.nextInt(15-1)-1;i++){
				day = r.nextInt(32-1)-1;
				month = r.nextInt(13-1)-1;
				date = Integer.toString(day) +"-" + Integer.toString(month) +"-2016";
				
			} 
		 */
		
	public Account selectAccount(int pAccountNumber){

		for(Account account : accounts){
			if(account.getAccountNumber() == pAccountNumber){
				return account;
			}
		}
		return null;
	}
	
	public void makeMovement(int type) throws NumberFormatException, ParseException, IOException{
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		Account account = null;
		String opcion;
		System.out.print("Digite el numero de cuenta: ");
		opcion = buffer.readLine();
		account = selectAccount(Integer.parseInt(opcion));
		if(account!=null){
			System.out.print("Digite la cantidad: ");
			opcion = buffer.readLine();
			if(type==1)
				account.withdraw( Double.parseDouble(opcion));
			else if(type==2)
				account.store( Double.parseDouble(opcion));
			else
				account.purchase( Double.parseDouble(opcion));
		}

			
	}
	
}
