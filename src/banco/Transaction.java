package banco;


import java.util.Date;


public class Transaction {
	static private int globalTransaction = 0;
	private int idTransaction;
	private Date date;
	private double amount;
	private boolean exemptComission;
	private int operation;
	
	private final int RETIROBANK = 0;
	private final int DEPOSITO = 1;
	private final int PAGODERECHOSDEESTUDIO = 2; 
	private final int COMISION = 3;
	private final int COMPRA  = 4;
	
	
	
	public Transaction(double pAmount,int pOperation, Date pDate){
		idTransaction = globalTransaction;
		globalTransaction++;
		date = pDate;
		amount = pAmount;
		exemptComission = false;
		operation = pOperation;
		
	}
	//setter and getters
	public int getIdTransaction() {
		return idTransaction;
	}
	public void setIdTransaction(int idTransaction) {
		this.idTransaction = idTransaction;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public boolean isExemptComission() {
		return exemptComission;
	}
	public void setExemptComission(boolean exemptComission) {
		this.exemptComission = exemptComission;
	}
	public String getDate() {
		return date.toString();
	}
	public String getTransactionType(){
		switch (operation) {
	        case RETIROBANK:  return "Retiro";
	                 
	        case DEPOSITO:  return "Deposito";
	                 
	        case PAGODERECHOSDEESTUDIO:  return "Pago de estudios";
	                 
	        case COMISION:  return "Pago de comision";
	                 
	        case COMPRA:  return "Compra";
	        default: return "Miscelaneo";
	                 
	    }
	}
	
	public String getCommissionCharge(){
		if (exemptComission){
			return "Yes";
		}else{
			return "No";
		}
	}
	
}
