package banco;
import java.util.Date;

public class Regular  extends Account{
	private double percentage;
	private int maxOperation;
	private double interestRate;

	public Regular(Client pIdClient, double pPercentage, double pInterestRate, int pMaxOperation ) {
		super(pIdClient);
		percentage = pPercentage;
		maxOperation = pMaxOperation;
		interestRate = pInterestRate;
	}

	@Override
	public String getTipoCuenta() {
		// TODO Auto-generated method stub
		return "Regular";
	}

	@Override
	public void commissionCharge() {
		this.setBalance(this.getBalance()-(this.getBalance()*this.interestRate));
		
	}

	@Override
	public void payInterest() {
		// TODO Auto-generated method stub
		this.setBalance(this.getBalance()+(this.getBalance()*this.interestRate));
		
	}
	


}
