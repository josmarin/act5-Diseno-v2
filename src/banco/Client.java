package banco;

public class Client {
	private String name;
	private String lastName;
	private int idClient;

	public Client(String pName, String pLastName, int pIdClient) {
		this.name = pName;
		this.lastName = pLastName;
		this.idClient = pIdClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	

}
