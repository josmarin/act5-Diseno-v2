package banco;

import java.util.Date;

public class Savings extends Account{
	private double savingsComission;
	private double interestRate;

	public Savings(Client pIdClient, double pComission, double pInterestRate ) {
		super(pIdClient);
		this.savingsComission = pComission;
		this.interestRate = pInterestRate;
	}

	public double getSavingsComission() {
		return comission;
	}

	public void setComission(double comission) {
		this.savingsComission = comission;
	}

	@Override
	public String getTipoCuenta() {
		return "Saving";
	}

	@Override
	public void commissionCharge() {
		this.setBalance(this.getBalance()+(this.getBalance()*this.interestRate));
		
	}

	@Override
	public void payInterest() {
		// TODO Auto-generated method stub
		this.setBalance(this.getBalance()+(this.getBalance()*this.interestRate));
		
		
		
	}

}
