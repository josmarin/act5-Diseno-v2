package banco;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public abstract class Account {
	static private int globalAccounts = 0;
	static int comission = 1;
	static int interestDay = 30;
	private int accountNumber;
	private Date creationDate;
	private double balance;
	private Client idClient;
	private State state;
	private Currency currency;
	private ArrayList<Transaction> transactions;
	private int STORE = 0;
	private int WITHDRAW = 1;
	private int PURCHASE = 2;

	Account(Client pClient) {
		transactions = new ArrayList<Transaction>();
		this.accountNumber = globalAccounts;
		globalAccounts ++;
		this.creationDate = new Date();
		this.currency = Currency.COLONES;
		this.state = State.ACTIVE;
		this.balance = 20000;
		this.idClient = pClient;
	}

	public static int getComission() {
		return comission;
	}

	public static void setComission(int comission) {
		Account.comission = comission;
	}

	public static int getInterestDay() {
		return interestDay;
	}

	public static void setInterestDay(int interestDay) {
		Account.interestDay = interestDay;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Client getIdClient() {
		return idClient;
	}

	public void setIdClient(Client idClient) {
		this.idClient = idClient;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public void withdraw(double amount) throws ParseException{
		if(amount<=balance){
			this.balance-=amount;
			transactions.add(new Transaction(amount,WITHDRAW, new Date()));
		}
		else{
			System.out.print("Saldo insuficiente");
		}
	}
	
	
	public void store(double amount) throws ParseException{
		this.balance+=amount;
		transactions.add(new Transaction(amount,STORE, new Date()));

		
	}
	
	public void purchase(double amount) throws ParseException{
		if(amount<=this.balance){
			this.balance-=amount;
			transactions.add(new Transaction(amount,PURCHASE, new Date()));
		}
		
	}
	
	
	public void printTransactions(){
		System.out.println("Account movements \n");
		System.out.println("--------------------------\n");
		for (Transaction t : transactions){
			System.out.println("Transaction id : " + t.getIdTransaction() + " Type : " +
					t.getTransactionType() + " Date : " + t.getDate() +
					" Amount : " + t.getAmount() +
					" Commission charge : " + t.getCommissionCharge() + "\n");
		}
		System.out.println("--------------------------\n");
		System.out.println("Current Balance : "+ this.balance + "\n");
		System.out.println("--------------------------\n");
	}
	
	public abstract String getTipoCuenta();
	
    public abstract void commissionCharge();

    public abstract void payInterest();

	@Override
	public String toString() {
		return "Numero Cuenta: " + accountNumber + " | Creacion: " + creationDate + ",| Balance: " + balance
				+ "| Moneda: " + currency;
	}
		
	

}
